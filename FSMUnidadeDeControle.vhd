----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:21:53 11/17/2015 
-- Design Name: 
-- Module Name:    FSM_UnidadeDeControle - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions:  
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM_UnidadeDeControle is
    Port ( clk2: in STD_LOGIC;
			 -- reset: in STD_LOGIC;
			  from_IR : in  STD_LOGIC_VECTOR(15 downto 0); -- contm o op code, operandos e registradores fonte e destino: xxxx xxxx xxxx xxxx
          		  PC_clr : out  STD_LOGIC;
           		  I_rd : out  STD_LOGIC;
          		  PC_inc : out  STD_LOGIC;
			  PC_ld : out STD_LOGIC;
           		  IR_ld : out  STD_LOGIC;
          		  D_addr : out  STD_LOGIC_VECTOR(7 downto 0);
			  D_rd : out  STD_LOGIC;
			  D_wr: out STD_LOGIC;
			  RF_s1 : out  STD_LOGIC;
			  RF_s0 : out STD_LOGIC;
			  RF_W_addr : out  STD_LOGIC_VECTOR(3 downto 0);
			  RF_W_wr : out  STD_LOGIC;
			  RF_Rp_addr : out  STD_LOGIC_VECTOR(3 downto 0);
			  RF_Rp_rd : out  STD_LOGIC;
			  RF_Rq_addr : out  STD_LOGIC_VECTOR(3 downto 0);
			  RF_Rq_rd : out  STD_LOGIC;
			  RF_Rp_zero: in STD_LOGIC;
			  alu_s1 : out  STD_LOGIC;
			  alu_s0 : out  STD_LOGIC;
			  rf_w_data: out std_logic_vector(7 downto 0)
			 );   
end FSM_UnidadeDeControle;

architecture Behavioral of FSM_UnidadeDeControle is
		-- Definio dos estados (Gray):
		-- S0: 0000: Incio
		-- S1: 0001: Busca
		-- S2: 0011: Decodificao
		-- S3: 0010: Carregar
		-- S4: 0110: Armazenar
		-- S5: 0111: Somar
		-- S6: 0101: Carregar constante
		-- S7: 0100: Subtrair
		-- S8: 1100: Saltar se zero
		-- S9: 1101: Saltar
		
		type estados is (S0,S1,S2,S3,S4,S5,S6,S7,S8,S9);
		signal estado: estados;
		
		--Entradas externas:
		--signal d: STD_LOGIC_VECTOR(7 downto 0);
		--signal ra: STD_LOGIC_VECTOR(3 downto 0);
		--signal rb: STD_LOGIC_VECTOR(3 downto 0);
		--signal rc: STD_LOGIC_VECTOR(3 downto 0);
	
begin

Controle: process (clk2)
	begin
		--if rf_rp_zero = '1' then
			--estado <= S0;
		if rising_edge(clk2) then
		--Descrio dos estados
		case estado is
			when S0 => --Incio
				PC_clr <= '1';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_addr <= "00000000";
				D_rd <= '0';
				D_wr <= '0';
				RF_s1 <= '0';
				RF_s0 <= '0';
				RF_W_addr <= "0000";
				RF_W_wr <= '0';
				RF_Rp_addr <= "0000";
				RF_Rp_rd <= '0';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
				--RF_Rp_zero <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';				
				estado <= S1;
				
			when S1 => --Busca 
				PC_clr <= '0';
				PC_ld <= '0';
				D_addr <= "00000000";
				D_rd <= '0';
				D_wr <= '0';
				RF_s1 <= '0';
				RF_s0 <= '0';
				RF_W_addr <= "0000";
				RF_W_wr <= '0';
				RF_Rp_addr <= "0000";
				RF_Rp_rd <= '0';	
				I_rd <= '1';
				PC_inc <= '1';
				IR_ld <= '1';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
				--RF_Rp_zero <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';	
				estado <= S2;
			when S2 => --Decodificao
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_addr <= "00000000";
				D_rd <= '0';
				D_wr <= '0';
				RF_s1 <= '0';
				RF_s0 <= '0';
				RF_W_addr <= "0000";
				RF_W_wr <= '0';
				RF_Rp_addr <= "0000";
				RF_Rp_rd <= '0';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
			--	RF_Rp_zero <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';				
				if ((from_IR(15 downto 12)) = "0000") then estado <= S3;
				elsif ((from_IR(15 downto 12)) = "0001")  then estado <= S4;
				elsif ((from_IR(15 downto 12)) = "0010")  then estado <= S5;
				elsif ((from_IR(15 downto 12)) = "0011")  then estado <= S6;
				elsif ((from_IR(15 downto 12)) = "0100")  then estado <= S7;
				elsif ((from_IR(15 downto 12)) = "0101")  then estado <= S8;
				end if;
			when S3 => --Carregar
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_wr <= '0';
				RF_W_addr <= from_IR(11 downto 8);
				RF_Rp_addr <= "0000";
				RF_Rp_rd <= '0';		
				D_addr <= from_IR(7 downto 0);
				D_rd <= '1';
				RF_s1 <= '0';
				RF_s0 <= '1';
				RF_W_wr <= '1';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
				--RF_Rp_zero <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';	
				estado <= S1;
			when S4 => --Armazenar
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_rd <= '0';
				RF_W_addr <= "0000";
				RF_W_wr <= '0';
				D_addr <= from_IR(7 downto 0);
				D_wr <= '1';
				RF_s1 <= '1'; -- Don't care
				RF_s0 <= '1'; -- Don't care
				RF_Rp_addr <= from_IR(11 downto 8);
				RF_Rp_rd <= '1';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
				--RF_Rp_zero <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';	
				estado <= S1;
			when S5 => --Somar
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_addr <= "00000000";
				D_rd <= '0';
				D_wr <= '0';	
				--RF_Rp_zero <= '0';
				RF_Rp_addr <= from_IR(7 downto 4);
				RF_Rp_rd <= '1';
				RF_s1 <= '0';
				RF_s0 <= '0';
				RF_Rq_addr <= from_IR(3 downto 0);
				RF_Rq_rd <= '1';
				RF_W_addr <= from_IR(11 downto 8);
				RF_W_wr <= '1';
				alu_s1 <= '0';
				alu_s0 <= '1';
				estado <= S1;
			when S6 =>  --Carregar constante
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_addr <= from_IR(7 downto 0);
				D_rd <= '0';
				D_wr <= '0';
				RF_Rp_addr <= "0000";
				RF_Rp_rd <= '0';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
				--RF_Rp_zero <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';	
				RF_s1 <= '1';
				RF_s0 <= '0';
				RF_W_addr <= from_IR(11 downto 8);
				RF_W_wr <= '1';
				estado <= S1;
			when S7 =>  --Subtrair
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_addr <= "00000000";
				D_rd <= '0';
				D_wr <= '0';
				--RF_Rp_zero <= '0';	
				RF_Rp_addr <= from_IR(7 downto 4);
				RF_Rp_rd <= '1';
				RF_s1 <= '0';
				RF_s0 <= '0';
				RF_Rq_addr <= from_IR(3 downto 0);
				RF_Rq_rd <= '1';
				RF_W_addr <= from_IR(11 downto 8);
				RF_W_wr <= '1';
				alu_s1 <= '1';
				alu_s0 <= '0';
				estado <= S1;
			when S8 => --Saltar se zero
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				PC_ld <= '0';
				IR_ld <= '0';
				D_addr <= "00000000";
				D_rd <= '0';
				D_wr <= '0';
				RF_s1 <= '0';
				RF_s0 <= '0';
				RF_W_addr <= "0000";
				RF_W_wr <= '0';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';	
				RF_Rp_addr <= from_IR(11 downto 8);
				RF_Rp_rd <= '1';
				if RF_Rp_zero = '0' then
					estado <= S9;
				else
					estado <= S1;
				end if;
			when S9 => --Saltar
				PC_clr <= '0';
				I_rd <= '0';
				PC_inc <= '0';
				IR_ld <= '0';
				D_addr <= "00000000";
				D_rd <= '0';
				D_wr <= '0';
				RF_s1 <= '0';
				RF_s0 <= '0';
				RF_W_addr <= "0000";
				RF_W_wr <= '0';
				RF_Rp_addr <= "0000";
				RF_Rp_rd <= '0';
				RF_Rq_addr <= "0000";
				RF_Rq_rd <= '0';
				--RF_Rp_zero <= '0';
				alu_s1 <= '0';
				alu_s0 <= '0';	
				PC_ld <= '1';
				estado <= S1;
		end case;
		end if;
		rf_w_data<=from_IR(7 downto 0);	
	end process Controle;
	
			
		

end Behavioral;
