library ieee;
use ieee.std_logic_1164.all;

entity registrador is
    port (
        clk : in  std_logic;
        --clr : in  std_logic;
        d : in  std_logic_vector (15 downto 0);
        ld1 : in  std_logic;
        q : out  std_logic_vector (15 downto 0)
    );
end registrador;

architecture Behavioral of registrador is

begin
 
    process (clk)
    begin
        if rising_edge(clk) then
            if ld1 = '1' then
                q <= "0000000000000000";
            elsif ld1 = '0' then
                q <= d;
            end if;
        end if;
    end process;

end Behavioral;