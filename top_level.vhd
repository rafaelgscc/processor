----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:39:27 12/06/2015 
-- Design Name: 
-- Module Name:    top_level - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_level is
 Port ( --clr: in std_logic;
 clkg: in std_logic;
			  addr : inout  STD_LOGIC_VECTOR (15 downto 0);
           rd : inout  STD_LOGIC;
           data1 : in  STD_LOGIC_VECTOR (15 downto 0);
           d_addr : out  STD_LOGIC_VECTOR (7 downto 0);
           d_rd : out  STD_LOGIC;
           d_wr : out  STD_LOGIC;
           rf_w_data : out  STD_LOGIC_VECTOR (7 downto 0);
           rf_s1n : out  STD_LOGIC;
           rf_s0n : out  STD_LOGIC;
           rf_w_addr : out  STD_LOGIC_VECTOR (3 downto 0);
           rf_w_wr : out  STD_LOGIC;
           rf_rp_addr : out  STD_LOGIC_VECTOR (3 downto 0);
           rf_rq_addr : out  STD_LOGIC_VECTOR (3 downto 0);
			  rf_rq_rd:out std_logic;
           rf_rp_zero : in  STD_LOGIC;
           alu_s1 : out  STD_LOGIC;
           alu_s0 : out  STD_LOGIC;
           rf_rp_rd : out  STD_LOGIC);
			  --rf_w_data: out std_logic_vector(7 downto 0));
end top_level;

architecture Behavioral of top_level is
component somador_1 is
Port ( a: in  STD_LOGIC_VECTOR(15 downto 0);
		 clk: in STD_LOGIC;
       b : in  STD_LOGIC_VECTOR(15 downto 0);
       out1 : out  STD_LOGIC_VECTOR(15 downto 0));
end component;

component PC is
PORT(input        : IN  std_logic_vector(15 downto 0);
         load,CLK,clr,up   : IN  std_logic;
         pc        : OUT std_logic_vector(15 downto 0));
			
end component;

component registrador is
 port (
        clk : in  std_logic;
        --clr : in  std_logic;
        d : in  std_logic_vector (15 downto 0);
        ld1 : in  std_logic;
        q : out  std_logic_vector (15 downto 0)
    );
end component;

component FSM_UnidadeDeControle is
    Port ( clk2: in STD_LOGIC;
			  --reset: in STD_LOGIC;
			  from_IR : in  STD_LOGIC_VECTOR(15 downto 0); -- contm o op code, operandos e registradores fonte e destino: xxxx xxxx xxxx xxxx
          		  PC_clr : out  STD_LOGIC;
           		  I_rd : out  STD_LOGIC;
          		  PC_inc : out  STD_LOGIC;
			  PC_ld : out STD_LOGIC;
           		  IR_ld : out  STD_LOGIC;
          		  D_addr : out  STD_LOGIC_VECTOR(7 downto 0);
			  D_rd : out  STD_LOGIC;
			  D_wr: out STD_LOGIC;
			  RF_s1 : out  STD_LOGIC;
			  RF_s0 : out STD_LOGIC;
			  RF_W_addr : out  STD_LOGIC_VECTOR(3 downto 0);
			  RF_W_wr : out  STD_LOGIC;
			  RF_Rp_addr : out  STD_LOGIC_VECTOR(3 downto 0);
			  RF_Rp_rd : out  STD_LOGIC;
			  RF_Rq_addr : out  STD_LOGIC_VECTOR(3 downto 0);
			  RF_Rq_rd : out  STD_LOGIC;
			  RF_Rp_zero: in STD_LOGIC;
			  alu_s1 : out  STD_LOGIC;
			  alu_s0 : out  STD_LOGIC;
			  rf_w_data: out std_logic_vector(7 downto 0)
			 );  
end component;
			 
signal fio_q: std_logic_vector(15 downto 0);
--signal fio_pc: std_logic_vector(15 downto 0);
signal fio_out: std_logic_vector(15 downto 0);
signal fio_clr: std_logic;
signal fio_load:std_logic;
signal fio_up: std_logic;
signal fio_ld: std_logic;	
		
begin
U11: somador_1 port map( clk=>clkg,
								  a=>fio_q,
								  b=>addr,
								  out1=>fio_out);


U12: PC  port map(
							CLK=>clkg,
							input=>fio_out,
							pc=>addr,
							load=>fio_load,
							clr=>fio_clr,
							up=>fio_up);
							
U13: registrador port map( clk=>clkg,
									--clr=>clr,
									d=>data1,
									ld1=>fio_ld,
									q=>fio_q);
	
	
U14: FSM_UnidadeDeControle port map(
												clk2=>clkg,
												--reset=>clr,
												PC_clr=>fio_clr,
												PC_inc=>fio_up,
												PC_ld=>fio_load,
												from_IR=>fio_q,
												I_rd=>rd,
												IR_ld=>fio_ld,
												D_addr=>d_addr,
												D_rd=>d_rd,
												D_wr=>d_wr,
												RF_s1=>rf_s1n,
												RF_s0=>rf_s0n,
												RF_W_addr=> rf_w_addr,
												RF_W_wr=>rf_w_wr,
												RF_Rp_addr=>rf_rp_addr,
												RF_Rq_addr=>rf_rq_addr,
												RF_Rq_rd=> rf_rq_rd,
												RF_Rp_zero=>rf_rp_zero,
												alu_s1=>alu_s1,
												alu_s0=>alu_s0,
												RF_rp_rd=>  rf_rp_rd,
												rf_w_data=>rf_w_data);
end Behavioral;

