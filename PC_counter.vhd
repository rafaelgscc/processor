----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:09:29 12/01/2015 
-- Design Name: 
-- Module Name:    PC_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

ENTITY PC IS
    PORT(input        : IN  std_logic_vector(15 downto 0);
         load,CLK,clr,up   : IN  std_logic;
         pc        : OUT std_logic_vector(15 downto 0));
END pc;

ARCHITECTURE Behavioral of pc IS
	signal p    :std_logic_vector(15 downto 0);
--signal temp :std_logic_vector(2 downto 0);
BEGIN
	PROCESS(clk)
BEGIN
--temp <=  up & clr & load ;
if(clk'event and clk = '1') then
	if(load='1' and clr = '0' and up ='0') then
		p <= input; pc <= p;
	elsif(load='0' and clr = '1' and up ='0') then
		p<="0000000000000000" ;pc <= p;
	elsif(load='0' and clr = '0' and up ='1') then
		p <= p + "0000000000000001";pc <= p;
	end if;		
--then
-- CASE temp IS
--   WHEN "001" => p <= input; pc <= p;
--   WHEN "010" => p<="0000000000000000" ;pc <= p;
--   WHEN "100" => p <= p + "0000000000000001";pc <= p;
--
--   WHEN OTHERS => pc <= "----------------";
-- END CASE;
end if;
END PROCESS;
END Behavioral;