library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity mux is
       Port ( clk : in  STD_LOGIC;
       		selecao: in  STD_LOGIC_VECTOR(1 DOWNTO 0);
          		in0 : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
           		in1 : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
        		in2_8b : in  STD_LOGIC_VECTOR(7 DOWNTO 0);
        		saida : out  STD_LOGIC_VECTOR(15 DOWNTO 0));
           
 end mux;
architecture Behavioral of mux is
  signal in2_16b:STD_LOGIC_VECTOR(15 DOWNTO 0);
begin
  process(clk)
      begin
                  
                  saida <= "0000000000000000";
  	   if (clk' event and clk = '1' ) then 
             
      	   if selecao = "00" then
         	 	saida <= in0;
       	 elsif selecao = "01"  then
            	saida <= in1;
      	  elsif selecao = "10" then
        	        if in2_8b = "0-------" then in2_16b(0) <= in2_8b(0) ;
        	        							  in2_16b(1) <= in2_8b(1) ;
        	        							  in2_16b(2) <= in2_8b(2) ;
        	        							  in2_16b(3) <= in2_8b(3) ;
        	        							  in2_16b(4) <= in2_8b(4) ;
        	        							  in2_16b(5) <= in2_8b(5) ;
        	        							  in2_16b(6) <= in2_8b(6) ;
        	        							  in2_16b(7) <= in2_8b(7) ;
        	        							  in2_16b(8) <= in2_8b(7) ;
        	        							  in2_16b(9) <= in2_8b(7) ;
        	        							  in2_16b(10) <= in2_8b(7) ;
        	        							  in2_16b(11) <= in2_8b(7) ;
        	        							  in2_16b(12) <= in2_8b(7) ;
        	        							  in2_16b(13) <= in2_8b(7) ;
        	        							  in2_16b(14) <= in2_8b(7) ;
        	        							  in2_16b(15) <= in2_8b(7) ;
        	        							  
        	        else 						  in2_16b(0) <= in2_8b(0) ;
        	        							  in2_16b(1) <= in2_8b(1) ;
        	        							  in2_16b(2) <= in2_8b(2) ;
        	        							  in2_16b(3) <= in2_8b(3) ;
        	        							  in2_16b(4) <= in2_8b(4) ;
        	        							  in2_16b(5) <= in2_8b(5) ;
        	        							  in2_16b(6) <= in2_8b(6) ;
        	        							  in2_16b(7) <= in2_8b(7) ;
        	        							  in2_16b(8) <= in2_8b(7) ;
        	        							  in2_16b(9) <= in2_8b(7) ;
        	        							  in2_16b(10) <= in2_8b(7) ;
        	        							  in2_16b(11) <= in2_8b(7) ;
        	        							  in2_16b(12) <= in2_8b(7) ;
        	        							  in2_16b(13) <= in2_8b(7) ;
        	        							  in2_16b(14) <= in2_8b(7) ;
        	        							  in2_16b(15) <= in2_8b(7) ;
        	        end if;
            saida <= in2_16b;
        end if;
      end if; 
   end process;
   
end Behavioral;
