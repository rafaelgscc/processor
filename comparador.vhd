library IEEE; use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

Entity comparador is
PORT (rp_data: IN std_logic_vector (15 downto 0);
	    rf_rp_zero : OUT std_logic);
End comparador;


architecture comparador of comparador is

begin 

rf_rp_zero <= '1' when rp_data = "0000000000000000" else '0';


end comparador;
