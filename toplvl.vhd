library IEEE; use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity top_lvl is
	generic (N: integer := 4; M: integer := 16);
	port(clk,
		rf_rp_rd,rf_rq_rd,rf_w_wr:	in	STD_LOGIC;
		rf_w_addr: in	STD_LOGIC_VECTOR(N-1 downto 0);
		rf_rp_addr: in	STD_LOGIC_VECTOR(N-1 downto 0);
		rf_rq_addr: in	STD_LOGIC_VECTOR(N-1 downto 0);
		r_data: 	in STD_LOGIC_VECTOR(M-1 downto 0);	
		rf_w_data: in STD_LOGIC_VECTOR(7 downto 0);	
		mux_sel:    in	 STD_LOGIC_VECTOR (1 downto 0);
		alu_sel:      in	 STD_LOGIC_VECTOR (1 downto 0);
		w_data: out STD_LOGIC_VECTOR(M-1 downto 0);	
		rf_rp_zero : OUT std_logic);
End;

architecture synth of top_lvl is

Component comparador is
PORT (rp_data: IN std_logic_vector (15 downto 0);
	    rf_rp_zero : OUT std_logic);
End Component;

Component ULA is
PORT (a,b: IN std_logic_vector (15 downto 0);
	    sel: IN std_logic_vector (1 downto 0);
	    x : OUT std_logic_vector (15 downto 0));
End component;

Component banco_reg is
	port(clk,
		rf_rp_rd,rf_rq_rd,rf_w_wr:	in	STD_LOGIC;
		rf_w_addr: in	STD_LOGIC_VECTOR(N-1 downto 0);
		rf_rp_addr: in	STD_LOGIC_VECTOR(N-1 downto 0);
		rf_rq_addr: in	STD_LOGIC_VECTOR(N-1 downto 0);
		w_data:	in	STD_LOGIC_VECTOR(M-1 downto 0);
		rq_data:    out STD_LOGIC_VECTOR(M-1 downto 0);
		rp_data:    out STD_LOGIC_VECTOR(M-1 downto 0));
End component;
		
component mux is
       Port ( clk : in  STD_LOGIC;
       		selecao: in  STD_LOGIC_VECTOR(1 DOWNTO 0);
          		in0 : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
           		in1 : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
        		in2_8b : in  STD_LOGIC_VECTOR(7 DOWNTO 0);
        		saida : out  STD_LOGIC_VECTOR(15 DOWNTO 0));
           
 end component;
 
 signal s_Rp_data, s_Rq_data,s_W_data, s_x: STD_LOGIC_VECTOR(15 downto 0);
  
 begin
 
 multiplex: mux port map (clk, mux_sel, s_x, r_data, rf_w_data, s_W_data);
 registrador: banco_reg port map (clk,rf_rp_rd,rf_rq_rd,rf_w_wr,rf_w_addr,rf_rp_addr,rf_rq_addr,s_W_data,s_Rq_data,s_Rp_data);
 ALU: ULA port map (s_Rp_data,s_Rq_data,alu_sel,s_x);
 Comp: comparador port map (s_Rp_data, rf_rp_zero);
 
 w_data <= s_Rp_data;
 
 
 end;
 
