library IEEE; use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

Entity ULA is
PORT (a,b: IN std_logic_vector (15 downto 0);
	    sel: IN std_logic_vector (1 downto 0);
	    x : OUT std_logic_vector (15 downto 0));
End ULA;

architecture ula of ula is
	
	begin
	
	with sel select
		x <= a  when "00",
	         a + b when "01",
	         a - b  when "10",
	         b 	when "11",
	         "0000000000000000" when others;
	
	
end ula;
	         
